/*
 Name: Changtong Qiu
 PID: A99076567
 DATE: 2/12/2016
 ASSIGNMENT: PA3
 */

#include "util.hpp"
#include "DictionaryTrie.hpp"

/* */
class DictionaryTrie::MultiTrieNode {
  
  class PredictWordComparator {
  public:
    bool operator()(MultiTrieNode* lhs, MultiTrieNode* rhs) const {
      if (lhs->freq == rhs->freq) {
        return lhs->word.compare(rhs->word) < 0 ? true : false;
      }
      return lhs->freq < rhs->freq;
    }
  };
  
public:
  bool isValue;
  unsigned int freq;
  string word;
  // a-z + (space)
  vector<MultiTrieNode*> routes = vector<MultiTrieNode*>(27, (MultiTrieNode*) 0);
  // a list of words as the result of prediction
  priority_queue<MultiTrieNode*, vector<MultiTrieNode*>, PredictWordComparator> predict;
  
  MultiTrieNode(bool value): isValue(value) {
  }
  
  MultiTrieNode* operator[](const int & pos) {
    return routes[pos];
  }
};

/* */
// TODO: could be faster
class DictionaryTrie::MultiTrie {
  int getPosition(const char & c) const {
    if (c == ' ') {
      return 26;
    }
    else if ((int)c >= 97 && (int)c <= 122){
      return (int)c - 97;
    }
    else {
      // invalid charactor received
      printf("invalid input\n");
      return -1;
    }
  }
  
public:
  MultiTrieNode* root;
  
  MultiTrie() {
    root = new MultiTrieNode(false);
  }
  
  
  MultiTrieNode* insert(MultiTrieNode* cur, const string & word, const unsigned int && index, const unsigned int & freq) {
    if (index == word.size()) {
      // word is already in
      if (cur->isValue) return (MultiTrieNode*) 0;
      cur->isValue = true; cur->freq = freq;
      cur->word = word; cur->predict.push(cur);
      return cur;
    }
    // TODO: assume there's no invalid input for insert
    const int & pos = getPosition(word[index]);
    if (cur->routes[pos] == 0) {
      // TODO: can be faster: change init
      cur->routes[pos] = new MultiTrieNode(false);
    }
    const auto result = insert(cur->routes[pos], word, index + 1, freq);
    // word is already in
    if (result == (MultiTrieNode*) 0) return result;
    cur->predict.push(result);
    return result;
  }
  
  bool insert(const string & word, const unsigned int & freq) {
    // invalid string passed in
    if (word.empty())
      return false;
    auto result = insert(root, word, 0, freq);
    // already exists
    if (result == 0) return false;
    return true;
  }
  
  bool find(string & word) const {
    auto cur = this->root;
    
    for(auto & ch : word) {
      const int & pos = getPosition(ch);
      if (cur->routes[pos] == 0 || pos == -1) {
        return false;
      }
      cur = cur->routes[pos];
    }
    
    if (cur->isValue == false) {
      return false;
    } else {
      return true;
    }
  }
  
  MultiTrieNode* getNode(string & word) const {
    auto cur = this->root;
    
    for(auto ch : word) {
      const int & pos = getPosition(ch);
      if (cur->routes[pos] == 0 || pos < 0) {
        return (MultiTrieNode*)0;
      }
      cur = cur->routes[pos];
    }
    return cur;
  }
};

/* Create a new Dictionary that uses a Trie back end */
DictionaryTrie::DictionaryTrie(){
  dictionary = new MultiTrie();
}

/* Insert a word with its frequency into the dictionary.
 * Return true if the word was inserted, and false if it
 * was not (i.e. it was already in the dictionary or it was
 * invalid (empty string) */
bool DictionaryTrie::insert(std::string word, unsigned int freq)
{
  return (*dictionary).insert(word, freq);
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryTrie::find(std::string word) const
{
  return (*dictionary).find(word);
}

/* Return up to num_completions of the most frequent completions
 * of the prefix, such that the completions are words in the dictionary.
 * These completions should be listed from most frequent to least.
 * If there are fewer than num_completions legal completions, this
 * function returns a vector with as many completions as possible.
 * If no completions exist, then the function returns a vector of size 0.
 * The prefix itself might be included in the returned words if the prefix
 * is a word (and is among the num_completions most frequent completions
 * of the prefix)
 */
std::vector<std::string> DictionaryTrie::predictCompletions(std::string prefix, unsigned int num_completions)
{
  std::vector<std::string> words;
  // error handling: num & empty prefix
  if (num_completions <= 0 || prefix.empty()) {
    printf("invalid request. exiting");
    return (vector<std::string>) 0;
  }
  // get to this node
  auto thisNode = (*dictionary).getNode(prefix);
//  // copy its list
//  auto predictPQ(thisNode->predict);
  // add them to the result array
  queue<MultiTrieNode*> temp;
  while (!thisNode->predict.empty() && words.size() < num_completions) {
    auto node = thisNode->predict.top();
    thisNode->predict.pop();
    temp.push(node);
    words.push_back(node->word);
  }
  while (temp.size() != 0) {
    thisNode->predict.push(temp.front());
    temp.pop();
  }
  return words;
}

/* Helper */
void DictionaryTrie::deleteAll(MultiTrieNode* root) {
  if( root == 0 ) return;
  for (auto each : root->routes) {
    if( each ) {
      deleteAll(each);
    }
  }
  delete root;
  root = 0;
}

/* Destructor */
DictionaryTrie::~DictionaryTrie() {
  deleteAll(dictionary->root);
  delete dictionary;
}

