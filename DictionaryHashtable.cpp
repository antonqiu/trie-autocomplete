/*
 Name: Changtong Qiu
 PID: A99076567
 DATE: 2/12/2016
 ASSIGNMENT: PA3
 */
#include "util.hpp"
#include "DictionaryHashtable.hpp"

/* Create a new Dictionary that uses a Hashset back end */
DictionaryHashtable::DictionaryHashtable(){}

/* Insert a word into the dictionary. */
bool DictionaryHashtable::insert(std::string word)
{
  return dictionary.insert(word).second;
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryHashtable::find(std::string word) const
{
  return dictionary.find(word) != dictionary.end();
}

/* Destructor */
DictionaryHashtable::~DictionaryHashtable(){}
