/*
 Name: Changtong Qiu
 PID: A99076567
 DATE: 2/12/2016
 ASSIGNMENT: PA3
 */

#include <stdio.h>
#include <fstream>
#include <set>
#include "DictionaryTrie.hpp"
#include "DictionaryBST.hpp"
#include "DictionaryHashtable.hpp"
#include "util.hpp"
#define REPEAT 10
using namespace std;

template <typename DictType>
void testDict(DictType * dictionary, int size, int step, int iter, ifstream & infile, Utils & util, set<string> & nondict) {
  Timer timer;
  
  for (int i = 0; i < iter; i++) {
    // TODO: print warning when there's no more?
    util.load_dict(*dictionary, infile, size + i * step);
    if (infile.eof()) {
      printf("Warning: All words in this dictionary were loaded\n");
      printf("Please discard result below this point\n");
    }
    // test worst case finding
    long long aveDuration = 0;
    for (int j = 1; j <= REPEAT; j++) {
      timer.begin_timer();
      for (auto & word : nondict) {
        (*dictionary).find(word);
      }
      aveDuration += timer.end_timer() / REPEAT;
    }
    
    printf("%d\t%lld\n", size + i * step, aveDuration);
  }
  printf("\n");
}


int main(int argc, char *argv[]) {
  
  if(argc < 5){
    std::cout << "Incorrect number of arguments." << std::endl;
    exit(-1);
  }
  int minSize = atoi(argv[1]);
  int stepSize = atoi(argv[2]);
  int numIter = atoi(argv[3]);
  
  if (numIter < 5 || minSize < 1000 || stepSize < 1000) {
    std::cout << "Invalid arguments" << std::endl;
    exit(-1);
  }
  
  ifstream infile;
  // input file process
  infile.open(argv[4]);
  if (!infile.is_open()) {
    cout << "Invalid input file.\n";
    return -1;
  }
  /* Uilt */
  Utils util;
  
  /* non-exist dict */
  set<string> nonexistDict {
    "a certified strength and conditioninga",
    "a certified strength and conditioningb",
    "a certified strength and conditioningc",
    "a certified strength and conditioningd",
    "a certified strength and conditioninge",
    "a certified strength and conditioningf",
    "a certified strength and conditioningg",
    "a certified strength and conditioningh",
    "a certified strength and conditioningi",
    "a certified strength and conditioningj"
  };
  // Alternatives
  set<string> nonexistDict2 {
    "a certified strength and conditioninga",
    "beginning of the clinton administrationb",
    "characteristics and behavioral tendenciesc",
    "electrical engineering and computer sciencee",
    "hitandrun attacks on the russianh",
    "in the upcoming presidential electioni",
    "large skillet over mediumhigh heatl",
    "maureen corrigan teaches literature atm",
    "of political attitudes and behavioro",
    "your local cooperative extension servicey"
  };
  auto dictTrie = new DictionaryTrie();
  auto dictBST = new DictionaryBST();
  auto dictHash = new DictionaryHashtable();
  
  printf("DictionaryTrie\n");
  testDict(dictTrie, minSize, stepSize, numIter, infile, util, nonexistDict);
  delete dictTrie; infile.close(); infile.open(argv[4]);
  
  printf("DictionaryBST\n");
  testDict(dictBST, minSize, stepSize, numIter, infile, util, nonexistDict);
  delete  dictBST; infile.close(); infile.open(argv[4]);
  
  printf("DictionaryHashtable\n");
  testDict(dictHash, minSize, stepSize, numIter, infile, util, nonexistDict);
  delete dictHash; infile.close();
  
  return 0;
}


