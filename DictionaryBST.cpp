/*
 Name: Changtong Qiu
 PID: A99076567
 DATE: 1/21/2016
 ASSIGNMENT: PA3
 */
#include "util.hpp"
#include "DictionaryBST.hpp"

/* Create a new Dictionary that uses a BST back end */
DictionaryBST::DictionaryBST(){}

/* Insert a word into the dictionary. */
bool DictionaryBST::insert(std::string word)
{
  auto callback = dictionary.insert(word);
  return callback.second;
}

/* Return true if word is in the dictionary, and false otherwise */
bool DictionaryBST::find(std::string word) const
{
  auto search = dictionary.find(word);
  return (search != dictionary.end());
}

/* Destructor */
DictionaryBST::~DictionaryBST(){}
